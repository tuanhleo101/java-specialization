package edu.coursera.parallel;

import edu.rice.pcdp.PCDP;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

import static edu.rice.pcdp.PCDP.*;


final class RowColumnRecursive extends RecursiveAction {

    double[][] A, B, C;
    int start, end;
    int N;
    boolean isForked;

    RowColumnRecursive(double[][] A, double[][] B, double[][] C, int start, int end, int N){
        this.A = A;
        this.B = B;
        this.C = C;
        this.start = start;
        this.end = end;
        this.N = N;
        this.isForked = true;
    }

    RowColumnRecursive(double[][] A, double[][] B, double[][] C, int start, int end, int N, boolean isForked){
        this.A = A;
        this.B = B;
        this.C = C;
        this.start = start;
        this.end = end;
        this.N = N;
        this.isForked = isForked;
    }

    @Override
    protected void compute() {
        if(isForked){
            forseq2d(start, end-1, 0, N-1, (i,j) -> {
                for(int k = 0; k < N; k++){
                    C[i][j] = A[i][k] * B[k][j];
                }
            });
        }else{
            ForkJoinTask.invokeAll(createSubtasks());
        }
    }

    private List<RowColumnRecursive> createSubtasks(){
        List<RowColumnRecursive> tasks = new ArrayList<>();
        int numCores = 8;
        int chunkSize = N / 8;
        for(int i = 0; i < numCores; i++){
            int start = chunkSize * i;
            int end = Math.min((i+1)*chunkSize, N);
            tasks.add(new RowColumnRecursive(A, B, C, start, end, N));
        }
        return tasks;
    }
}
/**
 * Wrapper class for implementing matrix multiply efficiently in parallel.
 */
public final class MatrixMultiply {
    /**
     * Default constructor.
     */
    private MatrixMultiply() {
    }

    /**
     * Perform a two-dimensional matrix multiply (A x B = C) sequentially.
     *
     * @param A An input matrix with dimensions NxN
     * @param B An input matrix with dimensions NxN
     * @param C The output matrix
     * @param N Size of each dimension of the input matrices
     */
    public static void seqMatrixMultiply(final double[][] A, final double[][] B,
            final double[][] C, final int N) {
        forseq2d(0, N - 1, 0, N - 1, (i, j) -> {
            C[i][j] = 0.0;
            for (int k = 0; k < N; k++) {
                C[i][j] += A[i][k] * B[k][j];
            }
        });
    }


    private static void forall2dChunkedMultiplication(final double[][] A, final double[][] B,
                                                final double[][] C, final int N, final int numCores){
        forall2dChunked(0, N - 1, 0, N - 1, numCores, (i, j) -> {
            C[i][j] = 0.0;
            for (int k = 0; k < N; k++) {
                C[i][j] += A[i][k] * B[k][j];
            }
        });
    }

    private static void splitBlockMultiplication(final double[][] A, final double[][] B,
                                                    final double[][] C, final int N, final int numCores){

        RowColumnRecursive task = new RowColumnRecursive(
                A, B, C, 0, N, N, false
        );
        ForkJoinPool pool = new ForkJoinPool(8);
        pool.invoke(task);
    }

    /**
     * Perform a two-dimensional matrix multiply (A x B = C) in parallel.
     *
     * @param A An input matrix with dimensions NxN
     * @param B An input matrix with dimensions NxN
     * @param C The output matrix
     * @param N Size of each dimension of the input matrices
     */
    public static void parMatrixMultiply(final double[][] A, final double[][] B,
            final double[][] C, final int N) {
        /*
         * TODO Parallelize this outermost two-dimension sequential loop to
         * achieve performance improvement.
         */

        // parallel
        forall2dChunkedMultiplication(A, B, C, N, PCDP.numThreads());
    }
}
